import './App.css';
import React, { useState } from 'react';
import ValidationComponent from './ValidationComponent';
import CharComponent from './CharComponent';

function App() {
  const [inputText, setInputText] = useState('');

  const removeCharHandler = (index) => {
    const text = inputText.split('');
    text.splice(index, 1);
    const updatedText = text.join('');
    setInputText(updatedText);
  }

  return (
    <div className="App">
      <input type="text" onChange={(e) => setInputText(e.target.value)} value={inputText} />
      <p>
        {"Length of entered text: " + inputText.length}
      </p>
      <ValidationComponent textLength={inputText.length} />
      {inputText.split('').map((el, index) => <CharComponent char={el} key={index} onClick={() => removeCharHandler(index)} />)}
    </div>
  );

}

export default App;
